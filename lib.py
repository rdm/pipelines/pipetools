#! /usr/bin/env python
# -*- coding: utf-8 -*-

from typing import Tuple

import numpy as np
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq


def writeParquet(df, fname):
    table = pa.Table.from_pandas(df)
    pq.write_table(table, fname)


def resultStack(saqc_result):
    variables = []
    for col in saqc_result.columns:
        var = saqc_result[col].round(8)
        var.index = pd.MultiIndex.from_product([[col], var.index])
        variables.append(var)
    return pd.concat(variables)


def splitData(df):
    if df.empty:
        return pd.DataFrame(), pd.DataFrame()
    data = df[["data"]]
    flags = df[["quality_flag", "quality_cause", "quality_comment"]]
    return data, flags


def mergeData(data, flags):
    data = data.copy()
    data.columns = pd.MultiIndex.from_product([data.columns, ["data"]])
    out = pd.concat([data, flags], axis="columns").sort_index(axis="columns")
    return out


def isDuplicated(left, right):
    """
    checks which of the values in `left` are also present in
    `right` and returns a boolean DataFrame with shape of `left`
    """
    idx = left.index.intersection(right.index)
    cols = left.columns.intersection(right.columns)
    lsub = left.loc[idx, cols].sort_index(axis="columns")
    rsub = right.loc[idx, cols].sort_index(axis="columns")
    mask = (lsub == rsub) | (lsub.isnull() & rsub.isnull())

    # I ran into this issue twice, so please NOTE:
    # The data upload through the DMP API is logger
    # and row based. That implies, that if we need
    # to upload one or more values from a given sensor,
    # we in fact need to upload all values with the
    # same timestamp for all other sensors as well.
    # This is wasteful, but we otherwise overwrite
    # valid DMP values with the `NaN`
    mask = mask.swaplevel()                   # swap (label, dates) -> (dates, label) for speed reasons
    unmask = (mask
              .all(axis=1)                    # check if all values in a given row are duplictes
              .groupby(level=0)               # groupby dates
              .all()                          # check if all values per date are duplicates
              .replace(True, np.nan).dropna() # remove groups, that only consist of duplicated values
              .index)                         # the index is enough to work on

    mask.loc[unmask] = False
    return mask.swaplevel().astype(bool).reindex(left.index, fill_value=False)


def maskDuplicates(df, base):
    if df.empty or base.empty:
        return df

    df = df.copy().sort_index(axis="index").sort_index(axis="columns")
    base = base.copy().sort_index(axis="index").sort_index(axis="columns")
    if df.equals(base):
        return pd.DataFrame()

    # some consistency checks
    for obj in [df, base]:
        if not isinstance(obj.index, pd.MultiIndex):
            raise ValueError("expected multi index input frames")
        if obj.index.get_level_values(1).inferred_type != "datetime64":
            raise ValueError("expected index level 1 of type date(time)")

    mask = isDuplicated(df, base)

    return df.drop(mask[mask.all(axis=1)].index)


def pivotTable(df, columns, values):
    # NOTE: a poor man's pivot_table, only faster and less memory hungry...
    groups = df.groupby(by=columns)
    out = pd.DataFrame()
    for k, v in groups:
        col = v[values]
        out[k] = col[~col.index.duplicated()]
    return  out
