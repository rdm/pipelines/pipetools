# DMPAPI

A Python module to access data from the UFZ Datamanagement Portal.

## Installation

To make the module locally available install the following packages, preferably of course, within a new virtual environment.

```shell
pip install -e git+https://git.ufz.de/rdm/pipelines/pipetools@master#egg=dmpapi
```

## Usage

### Initialisation

```python
>>> from dmpapi import DmpAPIPandas
>>> api = DmpAPIPandas(project_id, project_user, project_password)
```

### Data download

There are several data access methods:
- [`getSensorLevel1(sensor_id)`](https://git.ufz.de/rdm/pipelines/pipetools/-/blob/master/dmpapi.py#L305)

   Return the level1-data of a sensor identified by its id
- [`getSensorLevel2(sensor_id)`](https://git.ufz.de/rdm/pipelines/pipetools/-/blob/master/dmpapi.py#L247)

   Return the level2-data of a sensor identified by its id
- [`getLoggerLevel1(logger_id, start_date=None, end_date=None)`](https://git.ufz.de/rdm/pipelines/pipetools/-/blob/master/dmpapi.py#L282)

   Return the level1-data of an enire logger identified by it id, optionally limit the data downloaded
   to the time range from `start_date` to `end_date`
- [`getLoggerLevel2(logger_id, start_date=None, end_date=None)`](https://git.ufz.de/rdm/pipelines/pipetools/-/blob/master/dmpapi.py#L257)

   Return the level2-data of an enire logger identified by it id, optionally limit the data downloaded
   to the time range from `start_date` to `end_date`

The follwing snippet downloads the level2 data for a logger with id `9999` starting at 2020-01-01 in a single column format.
```python
>>> api.getLoggerLevel2(9999, start_date="2020-01-01")
```
If you prefer a more spreadsheet like data structure (which potentially holds artifically introduced `NaN` values) use:
```python
>>> api.getLoggerLevel2(9999, start_date="2020-01-01", stacked=False)
```

### Data upload

Due to the structure of the DMP data can only be uploaded to a specific logger, not to any given sensor. The following methods are available:
- `postLevel1Data(logger_id)`
- `postLevel2Data(logger_id)`
