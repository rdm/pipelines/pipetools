#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import json

from typing_extensions import Literal

import click

import numpy as np
import pandas as pd

from dmpapi import DmpAPIPandas, stripDaylightSavingTimes
from lib import splitData, isDuplicated, maskDuplicates


logging.basicConfig(format='[%(asctime)s][%(levelname)s] %(message)s', level=logging.INFO)


def maybeSplit(df, level: Literal["1", "2", "both"]):
    try:
        return splitData(df)
    except TypeError:
        if level == "1":
            return df, pd.DataFrame([])
        return pd.DataFrame([]), pd.DataFrame([])


@click.command()
@click.option("-i", "--infile", type=click.Path(exists=True), required=True)
@click.option("-p", "--project", type=int)
@click.option("-l", "--logger", type=int)
@click.option("-t", "--level", type=click.Choice(["1", "2", "both"]))
@click.option("--meta-data/--no-meta-data", default=True)
@click.option("-u", "--user", type=str, required=False)
@click.option("-p", "--password", type=str, required=False)
def main(infile, project, logger, level, meta_data, user, password):

    if user is None:
        user = os.environ["DMPUSER"]
    if password is None:
        password = os.environ["DMPPASS"]

    api = DmpAPIPandas(data_project=project, username=user, password=password)
    sensors = api.getSensors(logger)
    current_sensors = sensors.loc[
        (sensors["isDeleted"] == False) & (sensors["isUpdated"] == False)]

    df = pd.read_parquet(infile)
    df = stripDaylightSavingTimes(df)
    df = df.dropna(how="all", axis="index")

    cols = (df.index
            .get_level_values(0)
            .drop_duplicates()
            .intersection(current_sensors["sensorLabel"].drop_duplicates()))

    data_out, flags_out = maybeSplit(df.loc[cols], level)

    if meta_data is False:
        flags_out["quality_comment"] = json.dumps({"comment":"", "test": ""})

    if level in ("1", "both"):
        count = data_out.size
        if not data_out.empty:
            api.postLevel1Data(logger, data_out, label=True, verbose=True)

    if level in ("both", "2"):
        count = flags_out.size
        if not flags_out.empty:
            api.postLevel2Data(logger, flags_out, label=True, check=True, verbose=True)


if __name__ == "__main__":
    main()
