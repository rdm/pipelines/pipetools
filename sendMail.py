#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import smtplib
import ssl

import click

SERVER = "smtp.ufz.de"
PORT = 587

context = ssl.create_default_context()

@click.command()
@click.option("-f", "--sender", type=str, required=True)
@click.option("-t", "--recipient", type=str, required=True)
@click.option("-s", "--subject", type=str, required=True)
@click.option("-u", "--user", type=str, envvar="EMAILUSER")
@click.option("-p", "--password", type=str, envvar="EMAILPASS")
@click.option("--stdin/--no-stdin", type=bool, default=False)
def main(sender, recipient, subject, user, password, stdin):
    content = sys.stdin.read() if stdin else ""
    message = f"Subject: {subject}\n\n{content}"
    with smtplib.SMTP(SERVER, PORT) as server:
        server.starttls(context=context)
        server.login(user, password)
        server.sendmail(sender, recipient, message.encode("utf-8"))


if __name__ == "__main__":
    main()
