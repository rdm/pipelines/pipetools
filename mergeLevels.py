#! /usr/bin/env python
# -*- coding: utf-8 -*-

import click
import pandas as pd

from lib import writeParquet
from dmpapi import DmpAPIPandas

@click.command()
@click.option("-1", "--l1file", type=click.Path(exists=True), required=True)
@click.option("-2", "--l2file", type=click.Path(exists=True), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
def main(l1file, l2file, outfile):
    l1 = pd.read_parquet(l1file)
    l2 = pd.read_parquet(l2file)

    columns = ["data"] + list(DmpAPIPandas.MAP2API.keys())
    out = l1.copy()

    if not out.empty:

        # if l2 is empty, we want the respective fields anyhow
        if l2.empty:
            out = out.reindex(columns, axis="columns")
        else:
            out = out.join(l2.drop("data", axis=1))

    writeParquet(out, outfile)


if __name__ == "__main__":
    main()
