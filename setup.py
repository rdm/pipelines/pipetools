#! /usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dmpapi",
    version="0.8",
    author="David Schaefer",
    author_email="david.schaefer@ufz.de",
    description="Access layer to UFZ Datamanegement Portal Data Base",
    long_description=long_description,
    url="https://git.ufz.de/rdm/pipelines/pipetools/",
    py_modules=["dmpapi", "lib"],
    install_requires=["pandas", "pytz", "requests", "pyarrow"]
)
