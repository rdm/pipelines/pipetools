#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os

import click

from dmpapi import DmpAPIPandas
from lib import writeParquet


@click.command()
@click.option("-p", "--project", type=int, required=True)
@click.option("-l", "--logger", type=int, required=True)
@click.option("-t", "--level", type=click.Choice(["1", "2"]), required=True)
@click.option("-o", "--outfile", type=click.Path(), required=True)
@click.option("--user", type=str, required=False)
@click.option("--password", type=str, required=False)
@click.option("--cache/--no-cache", default=True)
def main(project, logger, level, outfile, user, password, cache):
    if user is None:
        user = os.environ["DMPUSER"]
    if password is None:
        password = os.environ["DMPPASS"]

    api = DmpAPIPandas(data_project=project, username=user, password=password)
    if level == "1":
        data = api.getLoggerLevel1(logger, label=True, cache=cache, stacked=True).sort_index(axis="index")
    else:
        data = api.getLoggerLevel2(logger, label=True, cache=cache, stacked=True).sort_index(axis="index")
    writeParquet(data, outfile)


if __name__ == "__main__":
    main()
