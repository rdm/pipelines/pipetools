#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import shelve
import pickle
import codecs
from pathlib import Path
from datetime import datetime, date

from joblib import Memory


def initCache(cache_path):
    cache_path = Path(cache_path)
    if cache_path.exists():
        ctime = datetime.fromtimestamp(cache_path.stat().st_ctime)
        midnight = datetime.combine(date.today(), datetime.min.time())
        if ctime < midnight:
            shutil.rmtree(str(cache_path))
    cache_path.mkdir(parents=True, exist_ok=True)


def joblibCache(cache_path):
    initCache(cache_path)
    def inner(func):
        mem = Memory(cache_path, verbose=False)
        return mem.cache(func)
    return inner


def shelveCache(cache_path):
    """
    NOTE:
    the joblib mechanism implemented in the function `cache`
    does not work with frozen python 'executables' (at least
    not with PyInstaller). That's why we use a simple `shelve`-
    based approach here.
    """
    initCache(cache_path)
    cache_file = Path(cache_path, "cache.shelve")
    def outer(func):
        def inner(*args, **kwargs):
            # NOTE:
            # we need to encode the bytes object to a string
            # in order to make it a shelve key. The usual decode
            # machinery did not work here, because of the structure
            # of the pickled object
            key = codecs.encode(pickle.dumps((args, kwargs)), "base64").decode()
            with shelve.open(str(cache_file)) as db:
                if key not in db:
                    db[key] = func(*args, **kwargs)
                return db[key]
        return inner
    return outer

