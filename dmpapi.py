#! /usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations

import os
import json
from typing import List, Dict, Any

import requests
import pytz

import pandas as pd
from lib import maskDuplicates


_APIHOST = "https://web-intern.app.ufz.de"
# _APIHOST = "https://webapp.intranet.ufz.de"
APIHOST = os.environ.get("APIHOST", _APIHOST)


class DmpAPI:

    _MAX_UPLOAD = 10_000
    _PARAMS_MAP = {
        "start_date": "startDate",
        "end_date": "endDate",
        "start": "start",
        "limit": "limit",
    }

    def __init__(self, data_project, username, password):
        self.username = username
        self.password = password
        self.host = f"{APIHOST}/dmpapi/v1/projects/{data_project}"

    def _mapParams(self, **params):
        out = {}
        for k, v in params.items():
            if k not in self._PARAMS_MAP:
                raise TypeError(
                    f"invalid parameter '{k}', expected one of {list(self._PARAMS_MAP.keys())}"
                )
            out[self._PARAMS_MAP[k]] = v
        return out

    def _downloadIter(self, url, **params):
        params = self._mapParams(**params)
        while True:
            res = requests.get(url, auth=(self.username, self.password), params=params)
            if res.status_code != 200:
                msg = self._genError(res)
                raise RuntimeError(msg)
            response = json.loads(res.text)
            d = response["data"]
            if d is None:
                break
            yield d
            link = response["pagination"]["next"]
            if link.strip() == "":
                break
            url = f"{APIHOST}/{link}"

    def _download(self, url, **params):
        data = []
        for chunk in self._downloadIter(url, **params):
            data.extend(chunk)
        return data

    def _upload(self, url, data: List[Dict]):
        while True:
            res = requests.post(
                url, auth=(self.username, self.password), json=data[: self._MAX_UPLOAD]
            )
            if res.status_code != 200:
                raise RuntimeError(self._genError(res))
            data = data[self._MAX_UPLOAD :]
            if not data:
                break

    def _genError(self, res: requests.Response):
        return f"{res.request.method} request to '{res.url}' failed with status code {res.status_code} and message:\n'{res.text.strip()}'"

    def getLoggers(self, **params):
        url = f"{self.host}/loggers"
        return self._download(url, **params)

    def getLoggerById(self, logger_id, **params):
        url = f"{self.host}/loggers/{logger_id}"
        return self._download(url, **params)

    def getLoggerByName(self, logger_label, **params):
        url = f"{self.host}/loggers/{logger_label}"
        return self._download(url, **params)

    def getLoggerLevel1(self, logger_id, **params):
        url = f"{self.host}/loggers/{logger_id}/data/level1"
        out = self._download(url, **params)
        return out

    def getLoggerLevel2(self, logger_id, **params):
        url = f"{self.host}/loggers/{logger_id}/data/level2"
        out = self._download(url, **params)
        return out

    def getLoggerLevel1Cached(self, logger_id, **params):
        url = f"{self.host}/loggers/{logger_id}/cache/level1"
        out = self._download(url, **params)
        return out

    def getLoggerLevel2Cached(self, logger_id, **params):
        url = f"{self.host}/loggers/{logger_id}/cache/level2"
        out = self._download(url, **params)
        return out

    def getSensors(self, logger_id=None, **params):
        if logger_id is None:
            url = f"{self.host}/sensors"
        else:
            url = f"{self.host}/loggers/{logger_id}/sensors"

        return self._download(url, **params)

    def getSensorById(self, sensor_id, **params):
        url = f"{self.host}/sensors/{sensor_id}"
        return self._download(url, **params)

    def getSensorLevel1(self, sensor_id, **params):
        url = f"{self.host}/sensors/{sensor_id}/data/level1"
        out = self._download(url, **params)
        return out

    def getSensorLevel2(self, sensor_id, **params):
        url = f"{self.host}/sensors/{sensor_id}/data/level2"
        out = self._download(url, **params)
        return out

    def getSensorLevel1Cached(self, sensor_id, **params):
        url = f"{self.host}/sensors/{sensor_id}/cache/level1"
        out = self._download(url, **params)
        return out

    def getSensorLevel2Cached(self, sensor_id, **params):
        url = f"{self.host}/sensors/{sensor_id}/cache/level2"
        out = self._download(url, **params)
        return out

    def getSensorByName(self, sensor_name, **params):
        url = f"{self.host}/sensors/{sensor_name}"
        return self._download(url, **params)

    def postLevel2Data(self, logger_id, data: List[Dict], **params):
        url = f"{self.host}/loggers/{logger_id}/data/level2"
        self._upload(url, data=data, **params)

    def postLevel1Data(self, logger_id, data: List[Dict], **params):
        url = f"{self.host}/loggers/{logger_id}/data/level1"
        self._upload(url, data=data, **params)


def getDaylightSavingTimes(dt_index, timezone):
    years = set(dt_index.year)
    out = []
    tz = pytz.timezone(timezone)
    for d in tz._utc_transition_times:
        if d.month == 3 and d.year in years:
            ts = pd.Timestamp(d).replace(hour=2)
            out.append(ts)
    return out


def stripDaylightSavingTimes(df, timezone="Europe/Berlin"):
    out = df.copy()
    if out.empty:
        return out

    dates = df.index
    if isinstance(dates, pd.MultiIndex):
        dates = dates.get_level_values(1)

    ds_times = getDaylightSavingTimes(dates, timezone)
    for dst in ds_times:
        idx = (dates < dst) | (dates >= dst + pd.Timedelta(hours=1))
        out = out[idx]
        dates = dates[idx]
    return out


class DmpAPIPandas(DmpAPI):

    MAP2DATA = {
        "value": "data",
        "qualityFlag": "quality_flag",
        "qualityCause": "quality_cause",
        "qualityComment": "quality_comment",
    }

    MAP2API = {
        "quality_flag": "qualityFlag",
        "quality_cause": "qualityCause",
        "quality_comment": "qualityComment",
    }

    def _download(self, url: str, **kwargs: Any) -> pd.DataFrame:
        data = super()._download(url, **kwargs)
        out = pd.DataFrame(data)
        return out

    def _upload(self, url, data: bytes):
        res = requests.post(url, auth=(self.username, self.password), data=data)
        if res.status_code != 200:
            raise RuntimeError(self._genError(res))

    def getSensors(self, logger_id=None, filter=True):
        out = super().getSensors(logger_id)
        if filter:
            out = out.loc[(out["isUpdated"] == False) & (out["isDeleted"] == False)]
        return out

    def postLevel1Data(self, logger_id, data: pd.DataFrame, label=False, verbose: bool | Callable[[str], None]=False):

        if not isinstance(data.index, pd.MultiIndex):
            data = data.stack(level=0).swaplevel()
            if isinstance(data, pd.Series):
                data = data.to_frame()

        data = data.rename({data.columns[0]: self.MAP2DATA["value"]}, axis="columns")

        base = self.getLoggerLevel1(logger_id, start_date=data.index.min(), end_date=data.index.max(), label=label)
        out = (
            data.round(8)
            .pipe(maskDuplicates, base.round(8))
            .rename({self.MAP2DATA["value"]: "value"}, axis="columns")
        )
        if verbose:
            if not callable(verbose):
                verbose = print
            verbose(f"uploading: {out.count().sum(axis=0)} L1 values")

        for out in self._prepPostData(out, label):
            super().postLevel1Data(logger_id, out)

    def postLevel2Data(self, logger_id, flags: pd.DataFrame, label=False, verbose: bool | Callable[[str], None]=False, check=True):

        if not isinstance(flags.index, pd.MultiIndex):
            flags = flags.stack(level=0).swaplevel()

        flags = flags.drop(
            self.MAP2DATA["value"], errors="ignore", axis="columns", inplace=False
        )

        if set(flags.columns) != set(self.MAP2API.keys()):
            raise ValueError(
                f"Expected a DataFrame with columns {self.MAP2API.keys()}, got {flags.columns.to_list()} instead"
            )

        base_flags = self.getLoggerLevel2(
            logger_id, start_date=flags.index.min(), end_date=flags.index.max(), label=label
        ).drop(
            self.MAP2DATA["value"], errors="ignore", axis="columns", inplace=False
        )

        out = maskDuplicates(flags, base_flags).rename(self.MAP2API, axis="columns")
        if check:
            # we can only upload L2 data, that has already L1 entries
            # -> don't produce aupload errors because of missing L1 data
            base_data = self.getLoggerLevel1(
                logger_id, start_date=flags.index.min(), end_date=flags.index.max(), label=label
            )
            index = out.index.intersection(base_data.index)
            out = out.loc[index]

        if verbose:
            if not callable(verbose):
                verbose = print
            verbose(f"uploading: {out.count().sum(axis=0)} L2 values")

        for out in self._prepPostData(out, label):
            super().postLevel2Data(logger_id, out)

    def getSensorLevel2(self, sensor_id: int, cache: bool = True) -> pd.DataFrame:
        data = (
            super().getSensorLevel2Cached(sensor_id)
            if cache
            else super().getSensorLevel2(sensor_id)
        )
        if data.empty:
            return data

        out = data.set_index("timestamp")[self.MAP2DATA.keys()].rename(self.MAP2DATA)
        out.index = pd.to_datetime(out.index)
        return out

    def getLoggerLevel2(
        self,
        logger_id: int,
        label: bool = False,
        cache: bool = True,
        start_date=None,
        end_date=None,
        stacked: bool = True,
    ) -> pd.DataFrame:

        if cache:
            data = super().getLoggerLevel2Cached(
                logger_id, start_date=start_date, end_date=end_date
            )
        else:
            data = super().getLoggerLevel2(
                logger_id, start_date=start_date, end_date=end_date
            )

        if data.empty:
            return data

        columns = "sensorLabel" if label is True else "sensorName"

        table = data.groupby(by=["timestamp", columns], as_index=False).last()
        table = table.rename(self.MAP2DATA, axis="columns")
        table.index = pd.MultiIndex.from_arrays(
            [table[columns], pd.to_datetime(table["timestamp"])],
            names=["varname", "timestamp"],
        )
        table = table[self.MAP2DATA.values()]

        if not stacked:
            table = (
                table.unstack(level="varname")
                .swaplevel(axis="columns")
                .sort_index(axis="columns")
            )

        return table

    def getLoggerLevel1(
        self,
        logger_id: int,
        label: bool = False,
        cache: bool = True,
        start_date=None,
        end_date=None,
        stacked: bool = True,
    ) -> pd.DataFrame:

        if cache:
            data = super().getLoggerLevel1Cached(
                logger_id, start_date=start_date, end_date=end_date
            )
        else:
            data = super().getLoggerLevel1(
                logger_id, start_date=start_date, end_date=end_date
            )

        if data.empty:
            return data

        columns = "sensorLabel" if label is True else "sensorName"

        table = data.groupby(by=["timestamp", columns], as_index=False).last()
        table.index = pd.MultiIndex.from_arrays(
            [table[columns], pd.to_datetime(table["timestamp"])],
            names=["varname", "timestamp"],
        )
        table = table.rename(self.MAP2DATA, axis="columns")[["data"]]
        if not stacked:
            table = (
                table.unstack(level="varname")
                .droplevel(0, axis="columns")
                .sort_index(axis="columns")
            )

        return table

    def getSensorLevel1(self, sensor_id, label=False, cache=True):
        data = (
            super().getSensorLevel1Cached(sensor_id)
            if cache
            else super().getSensorLevel1(sensor_id)
        )
        if data.empty:
            return data

        columns = "sensorLabel" if label is True else "sensorName"

        clean = data.groupby(by=["timestamp", columns], as_index=False).last()
        table = clean.pivot(index="timestamp", columns=columns, values="value")
        table.index = pd.to_datetime(table.index)

        return table

    def _prepPostData(self, data, label):

        if data.empty:
            return
        # sort by timestamp! This is important in order to upload
        # data timestep - wise (see comment in pipetools.lib.isDuplicated)
        data = data.sort_index(axis="index", level=[1, 0])
        data.index = data.index.set_names(
            ["sensorLabel" if label is True else "sensorName", "timestamp"]
        )

        # NOTE:
        # `to_dict` is quite expensive on large DataFrames, so let's do the chunking right here
        while not data.empty:

            chunk = data.iloc[: self._MAX_UPLOAD]
            data = data.iloc[self._MAX_UPLOAD :]
            _, split_date = chunk.index[-1]

            try:
                # we should not cut through singular timestamps
                overlap = data.loc[(slice(None), split_date), :]
                chunk = pd.concat([chunk, overlap]).sort_index(axis="index", level=[1, 0])
                data = data.drop(index=split_date, level=1)
            except KeyError:
                pass

            # chunk = chunk.dropna()
            chunk = chunk.reset_index(level=0)
            chunk.index = chunk.index.strftime("%Y-%m-%d %H:%M:%S")
            chunk = chunk.reset_index(level=0)
            chunk = chunk.to_json(orient="records").encode("utf-8")

            yield chunk
