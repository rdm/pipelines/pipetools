#! /usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import logging.handlers


class MySMTPHandler(logging.handlers.SMTPHandler):

    def __init__(self, name, *args, **kwargs):
        self.pipeline_name = name
        super().__init__(*args, **kwargs)

    def format(self, record):
        msg = super().format(record)
        content = getattr(record, "content", None)
        if content:
            msg = msg + "\n" + content
        return msg

    def getSubject(self, record):
        return "[{:}][{:}][{:}]".format(
            self.pipeline_name,
            logging.getLevelName(record.levelno),
            record.msg
        )


class PipeLogging:
    def __init__(self, name):
        self.name = name
        self.formatter = logging.Formatter(
            f"[%(asctime)s][{name}][%(levelname)s][%(message)s]"
        )
        logging.basicConfig(
            format = f"[%(asctime)s][{name}][%(levelname)s][%(message)s]"
        )

    def getLogger(self, name=""):
        return logging.getLogger(name)

    def __enter__(self):
        # NOTE:
        # custom handlers set during the lifetime of this
        # context manager are obviously lost...
        self._shadowed = {
            (logging, "addLevelName"): logging.addLevelName,
            (logging.getLogger(), "addHandler"): logging.getLogger().addHandler
        }
        for obj, name in self._shadowed:
            setattr(obj, name, lambda *arg: None)
        return self

    def __exit__(self, *args):
        for (obj, name), value in self._shadowed.items():
            setattr(obj, name, value)
        del self._shadowed
        return

    def setConsoleHandler(self, level="DEBUG"):
        handler = logging.StreamHandler()
        self._addHandler(handler, level)
        return self

    def setFileHandler(self, fname, level="INFO", backup_count=14):
        handler = logging.handlers.TimedRotatingFileHandler(
            filename=fname,
            when="midnight",
            backupCount=backup_count,
        )
        self._addHandler(handler, level)
        return self

    def setEmailHandler(self, sender, recipients, username, password, level="ERROR"):
        handler = MySMTPHandler(
            name=self.name,
            mailhost=("smtp.ufz.de", 587),
            fromaddr=sender,
            toaddrs=recipients,
            subject=f"[{self.name} Pipeline ERROR]",
            credentials=(username, password),
            secure=(),
        )
        self._addHandler(handler, level)
        return self

    def _addHandler(self, handler, level):
        handler.setLevel(level)
        handler.setFormatter(self.formatter)
        logging.getLogger().addHandler(handler)

