#! /usr/bin/env python
# -*- coding: utf-8 -*-

import click
import os
import owncloud

URL = "https://nc.ufz.de/"

@click.command()
@click.option("-i", "--infile", type=str, required=True)
@click.option("-o", "--outfile", type=click.Path(), required=False, default=None)
@click.option("-u", "--url", type=str, required=False, default=URL)
@click.option("--user", type=str, required=False, default=os.environ["NCUSER"])
@click.option("--password", type=str, required=False, default=os.environ["NCPASS"])
def main(url, infile, outfile, user=None, password=None):

    oc = owncloud.Client(url)
    oc.login(user_id=user, password=password)
    status = oc.get_file(infile, outfile)
    if status is not True:
        raise RuntimeError(f"failed to download file: {URL}{infile}")
    oc.logout()

if __name__ == "__main__":
    main()
